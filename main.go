package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
)

func main() {
	//route(endpoint) = "/fibonacci"
	http.HandleFunc("/fibonacci", httpHandler) //register the httpHandler to handle incoming HTTP requests

	//fibonacciPrinter(30)

	port := "8080" //default port
	fmt.Println("Server listening on port", port)
	err := http.ListenAndServe(":"+port, nil) //to start the server and listen for incoming HTTP requests
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

func fibonacciPrinter(num int, w http.ResponseWriter) {
	a := 0
	b := 1
	c := b
	fmt.Printf("Series is: %d %d", a, b)
	fmt.Fprintf(w, "Fibonacci series up to %d is: \n\n", num)
	fmt.Fprintln(w, a)
	for true {
		c = b
		b = a + b
		if c >= num {
			fmt.Println()
			break
		}
		a = c
		fmt.Fprintln(w, c)
		fmt.Printf(" %d", c)
	}
}

func httpHandler(w http.ResponseWriter, r *http.Request) {
	n := 30
	if r.URL.Query().Get("n") != "" {
		n, _ = strconv.Atoi(r.URL.Query().Get("n"))
	}
	fibonacciPrinter(n, w)
}
